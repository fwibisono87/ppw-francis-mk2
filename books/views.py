from django.shortcuts import render

# Create your views here.
def search(request):
    response = {}
    return render(request, "books/search.html", response)