from django.shortcuts import render, redirect
from .models import Matkul
from .forms import MatkulCreate
from django.http import HttpResponse


# Create your views here.

def index(request):
    daftarMatkul = Matkul.objects.all()
    return render(request, 'Matkul/daftar.html', {'shelf': daftarMatkul})


def upload(request):
    upload = MatkulCreate()
    if request.method == 'POST':
        upload = MatkulCreate(request.POST, request.FILES)
        if upload.is_valid():
            upload.save()
            return redirect('matkul')
        else:
            return HttpResponse("""tolong recheck formnya <a href = "{{ url : 'index'}}">reload</a>""")
    else:
        return render(request, 'Matkul/upload_form.html', {'upload_form': upload})


def update_Matkul(request, id):
    id = int(id)
    try:
        Matkul_sel = Matkul.objects.get(id=id)
    except Matkul.DoesNotExist:
        return redirect('matkul')
    Matkul_form = MatkulCreate(request.POST or None, instance=Matkul_sel)
    if Matkul_form.is_valid():
        Matkul_form.save()
        return redirect('matkul')
    return render(request, 'Matkul/upload_form.html', {'upload_form': Matkul_form})


def delete_Matkul(request, id):
    id = int(id)
    try:
        Matkul_sel = Matkul.objects.get(id=id)
    except Matkul.DoesNotExist:
        return redirect('matkul')
    Matkul_sel.delete()
    return redirect('matkul')
