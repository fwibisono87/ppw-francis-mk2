from django.db import models


class Matkul(models.Model):
    name = models.CharField(max_length = 30)
    dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=30)
    desc = models.CharField(max_length=150)
    kelas = models.CharField(max_length=20, default="NaN")
    semester = models.CharField(max_length=30)
    def __str__(self):
        return self.name