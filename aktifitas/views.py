from django.http import request
from django.shortcuts import redirect, render
from .forms import PeopleForm
from .models import People, Activities


def index(request):
    people = People.objects.all()
    activities = Activities.objects.all()
    context = {'people': people, 'activities': activities, 'page_title': 'Activities'}
    return render(request, 'Aktifitas/index.html', context)


def register(request):
    form = PeopleForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('/aktifitas')
    context = {'page_title': 'register', 'form': form}
    return render(request, "Aktifitas/register.html", context)
