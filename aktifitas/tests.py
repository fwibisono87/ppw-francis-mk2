from django.test import TestCase, Client
from .models import Activities, People


# Create your tests here.


class TestCaseStory(TestCase):

    def test_make_model(self):
        activities_obj = Activities.objects.create(
            activities_name="activities 1"
        )
        People.objects.create(
            people_name="people 1",
            activities=activities_obj
        )

    def test_root_url_check(self):
        response = Client().get('', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_register_url_check(self):
        response = Client().get('/aktifitas/register', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_views_check(self):
        response = Client().post("/aktifitas/register", follow=True, form={
            'people_name': 'people 1',
            'activities': 'activities 1'
        })
        self.assertEqual(response.status_code, 200)

    def test_model_check(self):
        activities_obj = Activities.objects.create(activities_name="activities 1")
        people_obj = People.objects.create(people_name="people 1", activities=activities_obj)

        activities_obj = Activities.objects.get(activities_name="activities 1")
        self.assertEqual(activities_obj.activities_name, "activities 1")

        people_obj = People.objects.get(people_name="people 1")
        self.assertEqual(people_obj.people_name, "people 1")
        self.assertEqual(people_obj.activities, activities_obj)
