$(".header_box > h2 > .title") .click(function (){
    let isi = $(this).parent().parent().next()
    console.log(isi)
    if (isi.hasClass("isUp")) {
        isi.slideToggle(100)
        isi.removeClass("isUp")
    }
    else{
        isi.slideUp(100)
        isi.toggleClass("isUp")
    }
})

$(".header_box > h2 > .up") .click(function (){
    let current = $(this).parent().parent().parent()
    let previous = $(this).parent().parent().parent().prev()

    console.log(current)
    console.log(previous)

    current.insertBefore(previous)
})

$(".header_box > h2 > .down") .click(function (){
    let tombol = $(this)
    let current = $(this).parent().parent().parent()
    let next = $(this).parent().parent().parent().next()

    console.log(current)
    console.log(next)

    current.insertAfter(next)
})