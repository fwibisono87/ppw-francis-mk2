from django.shortcuts import render
from datetime import datetime, date
import csv

nama = "Francis Wibisono"

description = open("static/description.txt", "r").read()
loremipsum = open("static/img/LoremIpsum.txt", "r").read()

with open("static/skills.txt", "r") as skill_file:
    skill_list = skill_file.readlines()

list_edu = []
edu_file = "static/education.csv"
with open(edu_file) as edu:
    reader = csv.reader(edu)
    for row in reader:
        list_edu.append(row)

with open("static/hobbies.txt", "r") as hobbies_file:
    hobbies_list = hobbies_file.readlines()

list_exp = []
exp_file = "static/experience.csv"
with open(exp_file) as exp:
    reader2 = csv.reader(exp)
    for rows in reader2:
        list_exp.append(rows)


def home(request):
    response = {'nama': nama, 'desc': description, "li": loremipsum, "skill": skill_list, "edu": list_edu, \
                'hobbies': hobbies_list, 'experience': list_exp, }
    return render(request, 'home.html', response)


def bonus(request):
    response = {}
    return render(request, 'bonus.html', response)

def old(request):
    response = {}
    return render(request, "old.html", response)