"""fwibisono87 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from fwibisono87 import views
from matkul import views as views2
from aktifitas import views as views3
from books import views as views4
from custom_login import views as views5

urlpatterns = [
    path('fwibisono87/', admin.site.urls),
    path('', views.home, name="home"),
    path('bonus', views.bonus, name="bonus"),
    path('old', views.old, name="old"),
    path('matkul', views2.index, name="matkul"),
    path('matkul/upload', views2.upload, name="upload-matkul"),
    path('matkul/update/<int:id>', views2.update_Matkul),
    path('matkul/delete/<int:id>', views2.delete_Matkul),
    path('aktifitas', views3.index, name="index"),
    path('aktifitas/register', views3.register, name="register"),
    path('search', views4.search),
    path('custom_login', views5.index, name="custom_login"),
    path('accounts/', include('django.contrib.auth.urls')),
]
