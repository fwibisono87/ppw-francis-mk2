from django.apps import AppConfig


class LoginConfig(AppConfig):
    name = 'custom_login'
